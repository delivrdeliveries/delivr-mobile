import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';


import LoggedTemplate from '../templates/LoggedTemplate';
import TouchableList from '../molecules/TouchableList';
import { getDeliveries } from '../requests/deliveries';
import DeliveryStateBadge from '../atoms/DeliveryStateBagde';
import { Icon } from 'react-native-elements';
import Colors from '../constants/Colors';
import { RowView } from '../atoms/Containers';
import { getLoggedId } from '../session/login'

export default class HomeScreen extends React.Component {

  static navigationOptions = {
    title: 'Entregas'
  };

  state = {
    deliveries: [],
    loading: []
  }

  componentDidMount() {
    this.loadDeliveries()
  }

  async loadDeliveries() {
    const id = await getLoggedId()
    const deliveries = await getDeliveries(id)
    this.setState({deliveries, loading: false})
  }

  _onPress (delivery) {
    const { navigate } = this.props.navigation
    navigate('Delivery', { delivery })
  }
  
  render() {
    return (
    <LoggedTemplate loading={this.state.loading} body={() => (
      <TouchableList 
        refreshing={this.state.loading}
        onRefresh={() => this.loadDeliveries()}
        data={this.state.deliveries.map(d =>( {...d, key: d.locator}))}
        onPress={item => this._onPress(item)} 
        renderItem={item => (
          <RowView>
            <DeliveryStateBadge state={item.status} /> 
            <Text>#{item.locator} - {item.load_description}</Text>
            <Icon name="chevron-right" color={Colors.dark} containerStyle={{position: 'absolute', right: 0, top: -5}}/>
          </RowView>
        )}
      />
    )} />
    );
  }

}