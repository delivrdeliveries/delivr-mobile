import React from 'react';
import LoggedTemplate from '../templates/LoggedTemplate';
import Colors from '../constants/Colors';
import { View, Dimensions } from 'react-native'
import DeliveryStateIcon from '../molecules/DeliveryStateIcon';
import DeliveryInfos from '../organisms/DeliveryInfos';
import { getDelivery } from '../requests/deliveries';
import SideSwipe from 'react-native-sideswipe';
import DeliveryEvents from '../organisms/DeliveryEvents';
import { DeliveryActions } from '../organisms/DeliveryActions';

export default class DeliveryScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const delivery = navigation.getParam('delivery')
    return {
      title: `Entrega #${delivery.locator}`,
      headerStyle: {
        backgroundColor: Colors.mainLight,
      },
      headerTintColor: Colors.dark,
    };
  };


  state = {
    delivery: undefined,
    loading: true
  }

  componentDidMount() {
    this.loadDelivery()
  }

  async loadDelivery() {
    const incompleteDelivery = this.props.navigation.getParam('delivery')
    const delivery = await getDelivery(incompleteDelivery.locator)
    this.setState({ delivery, loading: false })
  }

  onStartUpdateDelivery() {
    this.setState({loading: true})
  }

  onEndUpdateDelivery() {
    this.loadDelivery()
  }

  render() {
    const { delivery } = this.state

    return (
      <LoggedTemplate loading={this.state.loading} body={() => (
        <View style={{ flex: 1 }}>
          <DeliveryStateIcon state={delivery.status} style={{ flex: 3 }}/>

          <DeliveryActions 
            onStartUpdateDelivery={() => this.onStartUpdateDelivery()}
            onEndUpdateDelivery={() => this.onEndUpdateDelivery()}
            delivery={delivery} 
            style={{flex: 1}} />

          <SideSwipe 
            index={0}
            style={{ flex: 8 }}
            data={[
              <DeliveryInfos delivery={delivery} style={{ flex: 1 }} />,
              <DeliveryEvents events={delivery.events.reverse()} style={{ flex: 1 }}/>
            ]}
            renderItem={({item}) => {
              return (<View style={{ width: Dimensions.get('window').width }}>{item}</View>)
            }}
          />

        </View>
      )} />

    )
  }
}