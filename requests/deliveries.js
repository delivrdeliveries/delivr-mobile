import axios from 'axios'

const server = axios.create({
    baseURL: 'http://localhost:7000'
})

export async function getDeliveries(companyId) {
    return server.get(`/shipping_companies/${companyId}/deliveries`).then(r => r.data)
}

export async function getDelivery(locator) {
    return server.get(`/deliveries/${locator}`).then(r => r.data)
}

export async function postDeliveryEvent(locator, event) {
    return server.post(`/deliveries/${locator}/events`, event).then(r => r.data)
}