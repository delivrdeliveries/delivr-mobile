import React from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import Colors from '../constants/Colors';
import { formatDateTime } from '../date';
import { Text } from 'react-native-elements';
import types from '../constants/DeliveryEventTypes'
import { Strong } from '../atoms/StyledText';
import openMaps from 'react-native-open-maps'

export default class DeliveryEventCard extends React.Component {

    eventColors = {
        [types.CREATED]: Colors.mainLight,
        [types.SHIPPED]: Colors.warningOpaque,
        [types.LOCATION_UPDATED]: Colors.warningOpaque,
        [types.OCCURRENCE]: Colors.dangerOpaque,
        [types.DELIVERED]: Colors.warningOpaque,
        [types.FINISHED]: Colors.mainLight,
        [types.CANCELED]: Colors.dangerOpaque,
    }
    
    eventLabels = {
        [types.CREATED]: 'Criada',
        [types.SHIPPED]: 'Coletado',
        [types.LOCATION_UPDATED]: 'Localização Atualizada',
        [types.OCCURRENCE]: 'Em Ocorrência',
        [types.DELIVERED]: 'Entregue',
        [types.FINISHED]: 'Finalizada',
        [types.CANCELED]: 'Cancelada',
    }

    openMaps({latitude, longitude}) {
        openMaps({latitude: parseFloat(latitude), longitude: parseFloat(longitude), zoom: 20})
    }

    render() { 
        const { event } = this.props

        return (
            <TouchableOpacity style={styles.card} onPress={() => this.openMaps(event)}>
                <View style={[styles.detail, { backgroundColor: this.eventColors[event.type] } ]} />

                <View style={styles.content}>
                    <Strong>{this.eventLabels[event.type]}</Strong>
                    <Text style={styles.date}>{formatDateTime(event.created_at)}</Text>
                    <Text style={styles.description}>{event.description}</Text>
                </View>
            </TouchableOpacity>
        )
    }

}

const styles = StyleSheet.create({
    card: {
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: Colors.grayDark,
        flexDirection: 'row'
    },

    date: {
        fontSize: 10
    },

    header: {
        flexDirection: 'row'
    },

    detail: {
        width: 5,
        height: '100%',
        borderBottomLeftRadius: 5,
        borderTopLeftRadius: 5
    },

    description: {
        marginTop: 5,
    },

    content: {
        padding: 10
    }
})