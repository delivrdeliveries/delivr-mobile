import React from 'react'
import { Icon, Text } from 'react-native-elements'
import { View, StyleSheet } from 'react-native'
import states from '../constants/DeliverieStates'
import Colors from '../constants/Colors'

class StateIcon extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Icon
                    type='font-awesome'
                    size={30}
                    name={this.props.name}
                    color={Colors.light} 
                    containerStyle={[styles.icon, {backgroundColor: this.props.color}]}/>
                <Text style={styles.text}>{this.props.text}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    icon: {
        marginBottom: 10,
        marginLeft: 'auto',
        marginRight: 'auto',
        width: 50,
        height: 50,
        borderRadius: 100,
    },
    text: {
        textAlign: 'center',
    },
    container: {
        marginBottom: 20,
    }
})

const StateIconDelivered = () => <StateIcon name="archive" color={Colors.warning} text="Aguardando Confirmação" />
const StateIconFinished = () => <StateIcon name="check-circle" color={Colors.mainDark} text="Confirmada" />
const StateIconOnCarriage = () => <StateIcon name="truck" color={Colors.warning} text="Em Transporte" />
const StateAwaittingShipment = () => <StateIcon name="home" color={Colors.warning} text="Disponível para Retirada" />
const StateIconInOccurrence = () => <StateIcon name="exclamation-triangle" color={Colors.danger} text="Em Ocorrência" />
const StateIconCanceled = () => <StateIcon name="archive" color={Colors.danger} text="Cancelada" />

export default class DeliveryStateIcon extends React.Component {
    render(){
        switch (this.props.state) {
            case states.DELIVERED:
                return <StateIconDelivered />
            case states.FINISHED:
                return <StateIconFinished />
            case states.ON_CARRIAGE:
                return <StateIconOnCarriage />
            case states.IN_OCCURRENCE:
                return <StateIconInOccurrence />
            case states.CANCELED:
                return <StateIconCanceled />
            case states.AWAITTING_SHIPMENT:
                return <StateAwaittingShipment />
            default:
                return <StateIcon />
        }
    }
}