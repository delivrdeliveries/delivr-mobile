import React from 'react'
import { FlatList, TouchableHighlight, View, RefreshControl } from 'react-native'

export default class TouchableList extends React.Component {
    render() {
       return (
            <FlatList

            refreshControl={
                <RefreshControl
                    refreshing={this.props.refreshing}
                    onRefresh={this.props.onRefresh}
                />
            }

            data={this.props.data}
            renderItem={({item}) => (
            <TouchableHighlight
                onPress={() => this.props.onPress ? this.props.onPress(item) : undefined}>
                <View style={{
                    backgroundColor: 'white', 
                    padding: 20,
                    paddingLeft: 30,
                    paddingRight: 10
                }}>
                    {this.props.renderItem(item)}
                </View>
            </TouchableHighlight>
            )}
        />
       )
    }
}