import React from 'react'
import { View, Animated, Easing } from 'react-native'

const spinValue = new Animated.Value(0)

const animated = Animated.timing(
    spinValue,
  {
    toValue: 1,
    duration: 6000,
    easing: Easing.linear
  }
)

const spin = spinValue.interpolate({
  inputRange: [0, 1],
  outputRange: ['0deg', '360deg']
})

export default class Loader extends React.Component {

    componentDidMount() {
        animated.start()
        this.interval = setInterval(() => {
            animated.stop()
            animated.start()
        }, 6000)
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    render() {
        return (
            <View style={{
                display: 'flex',
                backgroundColor: '#fff',
                alignItems: 'center'
            }}>
                <Animated.Image 
                    style={{transform: [{rotate: spin}],
                        width: 50,
                        height: 50,
                        marginTop: '50%'
                    }}
                    source={require('../assets/images/delivr_isotype.png')} 
                />
            </View>
        )
    }
}