import moment from 'moment'

export const formatDate = date => {
    if (!date) {
        return
    } else if (date instanceof moment) {
        return date.format('DD/MM/YYYY')
    } else {
        return formatDate(moment(date))
    }
}

export const formatDateTime = dateTime => {
    if (!dateTime) {
        return
    } else if (dateTime instanceof moment) {
        return dateTime.format('DD/MM/YYYY HH:mm:ss')
    } else {
        return formatDateTime(moment(dateTime))
    }
}