import React from 'react'
import { ScrollView } from 'react-native'
import { ContentView } from '../atoms/Containers';
import DeliveryEventCard from '../molecules/DeliveryEventCard';

export default class DeliveryEvents extends React.Component {
    render() {
        return (
            <ContentView style={this.props.style}>
                <ScrollView>
                {this.props.events.map((event, idx) => <DeliveryEventCard key={idx} event={event} />)}
                </ScrollView>
            </ContentView>
        )
    }
}