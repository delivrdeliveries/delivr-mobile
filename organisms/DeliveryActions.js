import React from 'react'
import { View, Button, Alert } from 'react-native'
import deliveryStates from '../constants/DeliverieStates'
import deliveryEventTypes from '../constants/DeliveryEventTypes'
import Colors from '../constants/Colors';
import { postDeliveryEvent } from '../requests/deliveries';
import { getGeolocation } from '../geolocation';
import prompt from 'react-native-prompt-android';

export class DeliveryActions extends React.Component {

    async sendDeliveryEvent(message, event, description) {
        const { locator } = this.props.delivery
        this.props.onStartUpdateDelivery()
        const location = await getGeolocation()
        await postDeliveryEvent(locator, {...location, event, description})
        this.props.onEndUpdateDelivery()
        Alert.alert(message)
    }

    render() {
        const { delivery } = this.props

        if (delivery.status === deliveryStates.AWAITTING_SHIPMENT) {
            return (
                <View style={{ flex: 1 }}>
                    <Button title="Retirar" onPress={() => this.sendDeliveryEvent('Entrega retirada', deliveryEventTypes.SHIPPED)}/>
                </View>
            )
        } else if (delivery.status === deliveryStates.ON_CARRIAGE) {
            return (
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 20, paddingRight: 20 }}>
                    <Button title="Atualizar" onPress={() => this.sendDeliveryEvent('Localização atualizada', null, 'Localização atualizada via aplicativo')} />
                    
                    <Button title="Ocorrência" 
                    onPress={() => prompt(
                        'Comunicar Ocorrência',
                        'O que aconteceu?',
                        [
                        {text: 'Cancelar', style: 'cancel'},
                        {text: 'Confirmar', onPress: desc => this.sendDeliveryEvent('Entrega em ocorrência', deliveryEventTypes.OCCURRENCE, desc)},
                        ],
                        { type: 'plain-text' }
                    )} color={Colors.danger}/>
                    
                    <Button title="Finalizar"  onPress={() => prompt(
                        'Finalizar a entrega',
                        'Algum ponto a acrescentar? (opicional)',
                        [
                        {text: 'Cancelar', style: 'cancel'},
                        {text: 'Finalizar entrega', onPress: desc => this.sendDeliveryEvent('Entrega finalizada', deliveryEventTypes.DELIVERED, desc)},
                        ],
                        { type: 'plain-text' }
                    )} color={Colors.mainDark}/>
                </View>
            )
        }

        return (<View />)
    }
} 