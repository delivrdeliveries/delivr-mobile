import React from 'react'
import { ContentView } from '../atoms/Containers';
import { Text } from 'react-native-elements';
import { StyleSheet, Button, View, Alert } from 'react-native'
import { Strong, Link } from '../atoms/StyledText';
import formatAddress from '../addresses/formatAddress'
import openMaps from 'react-native-open-maps'
import { formatDateTime } from '../date';

export default class DeliveryInfos extends React.Component {

    openMaps(address) {
        openMaps({ query: formatAddress(address) })
    }

    openRoute(delivery) {
        openMaps({ 
            start: formatAddress(delivery.start_address),
            end: formatAddress(delivery.end_address)
        })
    }

    render() {
        const { delivery } = this.props
        return (
            <ContentView style={this.props.style}>

                <View style={styles.info}>
                    <Strong>Tipo do Produto:</Strong>
                    <Text> {delivery.product_type.name}</Text>
                </View>

                <View style={styles.info}>
                    <Strong>Restrição: </Strong>
                    <Text>{delivery.delivery_restriction.name}</Text>
                </View>
                
                <View style={styles.info}>
                    <Strong>Data Emissão: </Strong>
                    <Text>{formatDateTime(delivery.created_at)}</Text>
                </View>

                <View style={styles.address}>
                    <Strong>Endereço de retirada:</Strong>
                    <Link onPress={() => this.openMaps(delivery.start_address)}>{formatAddress(delivery.start_address)}</Link>
                </View>

                <View style={styles.address}>
                    <Strong>Endereço de entrega: </Strong>
                    <Link onPress={() => this.openMaps(delivery.end_address)}>{formatAddress(delivery.end_address)}</Link>
                </View>

                 <Button title="Ver Rota" onPress={() => this.openRoute(delivery)}/>
            </ContentView>
        )
    }
}

const styles = StyleSheet.create({
    info: {
        marginBottom: 10,
        marginTop: 10,
        flex: 2
    },

    address: {
        marginBottom: 10,
        marginTop: 10,
        flex: 3
    },
})