export const getGeolocation = () => new Promise((resolve, reject) => {

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            ({coords: { latitude, longitude }}) => resolve({latitude, longitude}),
            error => reject(error),
        )
    } else {
        reject(new Error('Dispositivo não suporta geolocation'))
    }
})