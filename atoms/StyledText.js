import React from 'react';
import { Text } from 'react-native';
import Colors from '../constants/Colors';

export class Strong extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, {
      fontWeight: '600',
    }]} />
  }
}

export class Link extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, {
      color: Colors.contrastDark,
      textDecorationLine: 'underline'
    }]} />
  }
}