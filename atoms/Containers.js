import React from 'react'

import { View, ScrollView } from 'react-native'
import Colors from '../constants/Colors';

export class MainView extends React.Component {
    render() {
        return (
            <View style={[this.props.style,
            {
                flex: 1,
                backgroundColor: '#fff',
            }]}>
                {this.props.children}
            </View>
        )
    }
}

export class BodyView extends React.Component {
    render() {
        return (
            <View style={[this.props.style,
            {
                paddingTop: 20
            }]}>
                {this.props.children}
            </View>
        )
    }
}

export class ContentView extends React.Component {
    render() {
        return (
            <View style={[this.props.style,
            {
                borderWidth: 10,
                borderColor: Colors.grayLight,
                marginRight: 10,
                marginLeft: 10,
                marginBottom: 10,
                padding: 10,
                borderRadius: 5
            }]}>
                {this.props.children}
            </View>
        )
    } 
}

export class RowView extends React.Component {
    render() {
        return <View style={{flexDirection: 'row'}}>{this.props.children}</View>
    }
}
