import React from 'react'
import { View } from 'react-native'
import states from '../constants/DeliverieStates'
import Colors from '../constants/Colors';

class BadgeIcon extends React.Component {
    render() {
        return (<View style={[this.props.style, {
            height: 15,
            width: 15,
            borderRadius: 15,
            marginRight: 15,
        }]}/>)
    }
}

const BadgeIconDelivered = () => <BadgeIcon style={{backgroundColor: Colors.warning}} />
const BadgeIconFinished = () => <BadgeIcon style={{backgroundColor: Colors.main}} />
const BadgeIconOnCarriage = () => <BadgeIcon style={{backgroundColor: Colors.warning}} />
const BadgeAwaittingShipment = () => <BadgeIcon style={{backgroundColor: Colors.warning}} />
const BadgeIconInOccurrence = () => <BadgeIcon style={{backgroundColor: Colors.danger}} />
const BadgeIconCanceled = () => <BadgeIcon style={{backgroundColor: Colors.danger}} />

export default class DeliveryBadgeBadge extends React.Component {
    render(){
        switch (this.props.state) {
            case states.DELIVERED:
                return <BadgeIconDelivered />
            case states.FINISHED:
                return <BadgeIconFinished />
            case states.ON_CARRIAGE:
                return <BadgeIconOnCarriage />
            case states.IN_OCCURRENCE:
                return <BadgeIconInOccurrence />
            case states.CANCELED:
                return <BadgeIconCanceled />
            case states.AWAITTING_SHIPMENT:
                return <BadgeAwaittingShipment />
            default:
                return <BadgeIcon />
        }
    }
}