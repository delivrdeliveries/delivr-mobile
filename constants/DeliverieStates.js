export default {
    DELIVERED: 'delivered',
    FINISHED:   'finished',
    CANCELED:   'canceled',
    ON_CARRIAGE: 'on_carriage',
    IN_OCCURRENCE: 'in_occurrence',
    AWAITTING_SHIPMENT: 'awaiting_shipment'
}