const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',

  main: '#30E582',
  mainDark: '#25B265',
  mainLight: '#77E9AB',
  grayLight: '#F5F5F5',
  grayDark: '#E3E3E3',
  warning: '#E2BD00',
  warningLight: '#FFDF3E',
  warningOpaque: '#FFEE95',
  danger: '#F76148',
  dangerLight: '#FF8571',
  dangerOpaque: '#FF9C8C',
  light: '#FFFFFF',
  dark: '#000000',
  contrastLight: '#DEFFEB',
  contrastDark: '#00441B',
};
