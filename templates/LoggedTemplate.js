import React from 'react'
import { MainView, BodyView } from '../atoms/Containers';
import Loader from '../molecules/Loader';

export default class LoggedTemplate extends React.Component {
    render() {
       return (
        <MainView>
            {
                this.props.loading ?
                    <Loader /> :
                    (
                        <BodyView style={{ flex: 1 }}>
                            {this.props.body()}  
                        </BodyView>
                    )
            }
        </MainView>
       )
    }
}